from pathlib import Path
import sys
from typing import List

import pandas as pd
from pandas import DataFrame
import numpy as np

from IPython import embed

from qlknn.models.ffnn import QuaLiKizNDNN
from qlknn.models.kerasmodel import megaHornNet

# Test the HornNet import and running
root = Path(__file__).parent.parent.resolve()
path_ETG = root / "ETG/nn.json"
path_ITG = root / "ITG/nn.json"
path_TEM = root / "TEM/nn.json"
nn = megaHornNet(
    [
        path_ETG,
        path_ITG,
        path_TEM,
    ]
)

# This assumes Karel-like folder structure
path_nml = root / "tests/test.nml"

input_styles = ["nml", "fake"]
outps = {}
safe_inps = {}
for ii, input_style in enumerate(input_styles):
    if input_style == "nml":
        import f90nml
        from f90nml import Namelist

        nml: Namelist = f90nml.read(path_nml)
        inp: List[List] = nml["test"]["input"]
        # Column names are by convension. This changes if source code changes..
        nml_feature_names = [
            "Zeff",
            "Ati",
            "Ate",
            "An",
            "q",
            "smag",
            "x",
            "Ti_Te",
            "logNustar",
            "gammaEB",
            "Te",
        ]
        inp = pd.DataFrame(inp, columns=nml_feature_names)
        inp.index.name = "dimx"
        inp_safe = inp.loc[:, nn._feature_names.tolist()]

    elif input_style == "fake":
        scann = 24
        inp = pd.DataFrame()
        inp["Ati"] = np.array(np.linspace(2, 13, scann))
        inp["Ate"] = np.full_like(inp["Ati"], 9.0)
        inp["An"] = np.full_like(inp["Ati"], 3.0)
        inp["q"] = np.full_like(inp["Ati"], 2)
        inp["smag"] = np.full_like(inp["Ati"], 1)
        inp["x"] = np.full_like(inp["Ati"], 0.449951)
        inp["Ti_Te"] = np.full_like(inp["Ati"], 1.0)
        inp["logNustar"] = np.full_like(inp["Ati"], -2.000217201545864)
        zeff = pd.DataFrame(np.full_like(inp["Ati"], 1), columns=["Zeff"])
        inp = pd.concat([zeff, inp], axis=1)
        inp_safe = inp

    safe_inps[ii] = inp_safe


def run_hornnets(safe_inputs):
    for ii, safe_inp in enumerate(safe_inputs):
        outp = nn.get_output(inp_safe)
        outp.index.name = "dimx"
        outps[ii] = outp
    return outps

def test_hornnet():
    outps = run_hornnets(safe_inps)
    for ii, safe_inp in enumerate(safe_inps):
        assert all(safe_inps[0] == safe_inp), f"Input {ii} does not match reference"

    for ii, outp in enumerate(outps):
        assert all(outps[0] == outp), f"Output {ii} does not match reference"

if __name__ == "__main__":
    outps = run_hornnets(safe_inps)
    print(f"All input styles {input_styles} result in the same in-and-output, namely:")
    print(safe_inps[0])
    print(outps[0])
