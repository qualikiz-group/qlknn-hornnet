These are QuaLiKiz neural networks based on a novel NN architecture.
They are developed by P. Horn et al. Currently the networks are
under testing, and a publication is in preparation.

For more information contact Karel van de Plassche on
k.l.vandeplassche@differ.nl or karelvandeplassche@gmail.com
